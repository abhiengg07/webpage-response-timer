require "thor"
require 'net/https'
require 'uri'
require 'benchmark'

module WebResponseTimer
  class HammerOfTheGods < Thor
    desc "status URL", "This will check the status of the URL"
    def status( url )
      uri = URI.parse(url)
      http = Net::HTTP.new(uri.host, uri.port)
      req = Net::HTTP::Get.new(uri.request_uri)
      http.use_ssl = true
      puts "Webpage Status Code =  #{http.request(req).code}"
    end
    
    desc "time URL", "This will log the response time of the URL to the console"
    long_desc <<-TIMER

    `time URL` will print out the HTTP response time to the URL of your choosing
    on to the console screen.

    TIMER
    def time( url )
      uri = URI.parse(url)
      http = Net::HTTP.new(uri.host, uri.port)
      req = Net::HTTP::Get.new(uri.request_uri)
      http.use_ssl = true
      time = Benchmark.realtime do
      response = http.request(req)
      end
      puts "*********************Response time = #{time*1000} ms*************************"
    end
  end
end
