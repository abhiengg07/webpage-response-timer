# WebResponseTimer

A Simple CLI tool to log the response time to any valid URL of your choice. The CLI tool also provides an option to check the status of the webpage.  

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'webResponseTimer'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install webResponseTimer

## Usage

```ruby
bundle exec bin/webResponseTimer [COMMAND] [URL]
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).


## Code of Conduct

Everyone interacting in the WebResponseTimer project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/[USERNAME]/webResponseTimer/blob/master/CODE_OF_CONDUCT.md).
